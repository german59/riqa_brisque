from modules import *
import pickle
import numpy as np
import matplotlib.pyplot as plt
import cv2
import libsvm.svmutil
import skimage.io
import skimage.color

def load_image(path):
    image = cv2.imread(path, cv2.IMREAD_COLOR)
    image = cv2.resize(image, (299,299))
    return cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
def plot_histogram(x, label):
    n, bins = np.histogram(x.ravel(), bins = 50)
    n = n / np.max(n)
    plt.plot(bins[:-1], n, label = label, marker = 'o')
def scale_features(features):
    with open('normalize.pickle', 'rb') as handle:
        scale_params = pickle.load(handle)

    min_ = np.array(scale_params['min_'])
    max_ = np.array(scale_params['max_'])

    return -1 + (2.0 / (max_ - min_) * (features - min_))

if __name__ == '__main__':
    path1 = '502_right.jpeg'
    path2 = '314_left.jpeg'
    image1 = load_image(path1)
    image2 = load_image(path2)
    gray_image1 = skimage.color.rgb2gray(image1)
    mscn_coefficients1 = calculate_mscn_coeficients(gray_image1, 7, 7/6)
    coefficients1 = calculate_pair_product_coefficients(mscn_coefficients1)
    gray_image2 = skimage.color.rgb2gray(image2)
    mscn_coefficients2 = calculate_mscn_coeficients(gray_image2, 7, 7/6)
    coefficients2 = calculate_pair_product_coefficients(mscn_coefficients2)
    plt.figure()
    plt.subplot(2, 2, 1)
    plt.imshow(image1)
    plt.subplot(2, 2, 2)
    for name, coeff in coefficients1.items():
        plot_histogram(coeff.ravel(), name)
    plt.subplot(2, 2, 3)
    plt.imshow(image2)
    plt.subplot(2, 2, 4)
    for name, coeff in coefficients2.items():
        plot_histogram(coeff.ravel(), name)
    plt.legend()
    plt.show()
    brisque_features = calculate_brisque_features(gray_image1, kernel_size=7, sigma=7/6)
    for x in zip(brisque_features):
        print(x)
